const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var mongoose = require("mongoose");
var session = require('express-session');


// app.use(bodyParser());
app.use(bodyParser.urlencoded({
    extended: false
}))


mongoose.connect("mongodb://localhost:27017/book1", {
    useNewUrlParser: true
}, function(err, db) {
    if (err) {
        console.log("Couldn't connect to database");
    } else {
        console.log(`Connected To Database`);
    }
});

var bookRoute = require('./Route/bookRoute');
var userRoute = require('./Route/userRoute');

app.use(express.static(__dirname + '/uploads'))
app.use('/user', userRoute);
app.use('/book', bookRoute);

app.use(
    session({
        secret: "iy98hcbh489n38984y4h498", // don't put this into your code at production.  Try using saving it into environment variable or a config file.
        resave: true,
        saveUninitialized: false
    })
);





var port = 6666;
app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});