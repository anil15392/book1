var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var userSchema = new Schema({
    name: {
        type: String
    },
    address: {
        type: String
    },
    phoneNumber: {
        type: Number
    },
    gender: {
        type: String
    },
    dateOfBirth: {
        type: String
    },
    password: {
        type: String
    },
    requestId: {
        type: String
    },
    otp: {
        type: Number
    },
    seqNo: {
        type: Number
    }
})
module.exports = mongoose.model('User', userSchema);