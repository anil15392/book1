var mongoose=require("mongoose");
var Schema=mongoose.Schema;
var bookSchema=new Schema({
	name:{
		type:String
	},
	seqNo:{
		type:String
	},
	author:{
		type:String
	},
	price:{
		type:Number
	},
	description:{
		type:String
	},
	userId:{
		ref:"User",
	    type:String 
	}
})
module.exports = mongoose.model('Book', bookSchema);