var multer = require('multer')
var path = require('path')

module.exports.file = function(req, res) {
    //config object
    var storage = multer.diskStorage({
        //destination is to specify the path to the directory where uploaded files will be stored.
        destination: function(req, file, callback) {
            callback(null, './uploads')
        },
        //filename is used to determine what the file should be named inside the folder.
        filename: function(req, file, callback) {
            callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    })

    var upload = multer({
        storage: storage,
        fileFilter: function(req, file, callback) {
            //built-in path Node.js library
            var ext = path.extname(file.originalname)
                //  req.fileName = file.originalname;
                //split() makes it an array, pop() takes off the end of the array, 
                // var ext = req.fileName.split('.').pop();
                //upload files only with png, jpg, gif and jpeg extensions.
            if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
                return callback(res.esnd('file formate not supported'), null)
            }
            callback(null, true)
        }
    }).single('userFile');
    upload(req, res, function(err) {
        res.send('File is uploaded')
    })
}



// methods.uploadImages = function(req, res) {
//     var fileName = "";
//     var storage = multer.diskStorage({
//         destination: function(req, file, cb) {
//             cb(null, imagePath)
//         },
//         filename: function(req, file, cb) {
//             fileName = Date.now() + '.' + file.originalname.split(".").pop();
//             console.log("fisrt", fileName)
//             cb(null, fileName)
//         }
//     });

//     var uploadfile = multer({
//         storage: storage
//     }).single('avatar');

//     uploadfile(req, res, function(err) {
//         if (err) {
//             response.error = true;
//             response.status = 500;
//             response.errors = err;
//             response.userMessage = "some error occurred in file uploading";
//             return SendResponse(res);
//         } else {
//             response.error = false;
//             response.status = 200;
//             response.errors = err;
//             response.data = "/images/" + fileName;
//             response.userMessage = "Profile image uploaded.";
//             return SendResponse(res);
//         }
//     });
// }

/*-----  End of uploadImages  ------*/

// methods.forgotPassword = function(req, res) {
//     req.checkBody('email', 'email is required.').notEmpty();
//     var errors = req.validationErrors(true);
//     if (errors) {
//         response.error = true;
//         response.status = 400;
//         response.errors = errors;
//         response.userMessage = 'Validation errors';
//         return SendResponse(res);
//     } else {
//         //Database functions here
//         Admin.findOne({
//             email: req.body.email
//         }).exec(function(err, result) {
//             if (err) {
//                 //send response to client
//                 response.error = true;
//                 response.status = 500;
//                 response.errors = err;
//                 response.userMessage = 'some server error has occurred.';
//                 response.data = null;
//                 return SendResponse(res);

//             } else if (!result) {
//                 //Database functions here
//                 User.findOne({
//                     email: req.body.email
//                 }).exec(function(err, result) {
//                     if (err) {
//                         //send response to client
//                         response.error = true;
//                         response.status = 500;
//                         response.errors = err;
//                         response.userMessage = 'some server error has occurred.';
//                         response.data = null;
//                         return SendResponse(res);

//                     } else if (!result) {
//                         //send response to client
//                         response.error = true;
//                         response.status = 400;
//                         response.errors = err;
//                         response.userMessage = 'no request found.';
//                         response.data = null;
//                         return SendResponse(res);

//                     } else {
//                         var newpassword = randomstring.generate(8)
//                         User.findOneAndUpdate({
//                             _id: result._id
//                         }, {
//                             $set: {
//                                 password: encrypt(newpassword)
//                             }
//                         }, {
//                             new: true
//                         }).exec(function(err, user) {
//                             if (err) {
//                                 response.error = true;
//                                 response.status = 500;
//                                 response.errors = err;
//                                 response.userMessage = "some server error has occurred.";
//                                 return SendResponse(res);
//                             } else {
//                                 var mailGenerator = new Mailgen({
//                                     theme: 'default',
//                                     product: {
//                                         // Appears in header & footer of e-mails
//                                         name: 'eMechanic',
//                                         link: 'http://www.cheaponshopjewish.com/',
//                                         logo: 'http://admin.cheaponshopjewish.com/assets/img/cheapon.png'
//                                     }
//                                 });
//                                 var email = {
//                                     body: {
//                                         name: result.fullName,
//                                         intro: "Greeting from eMechanic! You have received this mail as per your forgot password request and this is your new password \"" + newpassword + "\"",
//                                         outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.'
//                                     }
//                                 };
//                                 // Generate an HTML email with the provided contents
//                                 var emailBody = mailGenerator.generate(email);
//                                 notifications.sendMail(req.body.email, 'Forgot Password', emailBody)
//                                     // notifications.mail(req.body.email, newpassword)
//                                 response.error = false;
//                                 response.status = 200;
//                                 response.errors = err;
//                                 response.data = '';
//                                 response.userMessage = "new password sent to your email.";
//                                 return SendResponse(res);
//                             }
//                         });
//                     }
//                 })

//             } else {
//                 var newpassword = randomstring.generate(8)
//                 Admin.findOneAndUpdate({
//                     _id: result._id
//                 }, {
//                     $set: {
//                         password: encrypt(newpassword)
//                     }
//                 }, {
//                     new: true
//                 }).exec(function(err, user) {
//                     if (err) {
//                         response.error = true;
//                         response.status = 500;
//                         response.errors = err;
//                         response.userMessage = "some server error has occurred.";
//                         return SendResponse(res);
//                     } else {
//                         var mailGenerator = new Mailgen({
//                             theme: 'default',
//                             product: {
//                                 // Appears in header & footer of e-mails
//                                 name: 'eMechanic',
//                                 link: 'http://www.cheaponshopjewish.com/',
//                                 logo: 'http://admin.cheaponshopjewish.com/assets/img/cheapon.png'
//                             }
//                         });
//                         var email = {
//                             body: {
//                                 name: result.fullName,
//                                 intro: "Greeting from eMechanic! You have received this mail as per your forgot password request and this is your new password \"" + newpassword + "\"",
//                                 outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.'
//                             }
//                         };
//                         // Generate an HTML email with the provided contents
//                         var emailBody = mailGenerator.generate(email);
//                         notifications.sendMail(req.body.email, 'Forgot Password', emailBody)
//                             // notifications.mail(req.body.email, newpassword)
//                         response.error = false;
//                         response.status = 200;
//                         response.errors = err;
//                         response.data = '';
//                         response.userMessage = "new password sent to your email.";
//                         return SendResponse(res);
//                     }
//                 });
//             }
//         })
//     }
// }

// /*-----  End of Forgot Password  ------*/
/*============================
***   getDashboardData  ***
==============================*/
// methods.getDashboardData = function(req, res) {
//     //Check for POST request errors.
//     User.count(function(err, userCount) {
//         if (err) {
//             //send response to client
//             response.error = true;
//             response.status = 500;
//             response.errors = err;
//             response.userMessage = 'some server error has occurred.';
//             response.data = null;
//             return SendResponse(res);
//         } else {
//             Kit.count(function(err, kitCount) {
//                 if (err) {
//                     //send response to client
//                     response.error = true;
//                     response.status = 500;
//                     response.errors = err;
//                     response.userMessage = 'some server error has occurred.';
//                     response.data = null;
//                     return SendResponse(res);
//                 } else {
//                     Product.count(function(err, productCount) {
//                         if (err) {
//                             //send response to client
//                             response.error = true;
//                             response.status = 500;
//                             response.errors = err;
//                             response.userMessage = 'some server error has occurred.';
//                             response.data = null;
//                             return SendResponse(res);
//                         } else {
//                             Offer.count(function(err, offerCount) {
//                                 if (err) {
//                                     //send response to client
//                                     response.error = true;
//                                     response.status = 500;
//                                     response.errors = err;
//                                     response.userMessage = 'some server error has occurred.';
//                                     response.data = null;
//                                     return SendResponse(res);
//                                 } else {
//                                     Order.count(function(err, orderCount) {
//                                         if (err) {
//                                             //send response to client
//                                             response.error = true;
//                                             response.status = 500;
//                                             response.errors = err;
//                                             response.userMessage = 'some server error has occurred.';
//                                             response.data = null;
//                                             return SendResponse(res);
//                                         } else {
//                                             response.error = false;
//                                             response.status = 200;
//                                             response.errors = null;
//                                             response.userMessage = "login successfully.";
//                                             response.data = {
//                                                 user: userCount,
//                                                 kit: kitCount,
//                                                 product: productCount,
//                                                 offer: offerCount,
//                                                 order: orderCount
//                                             };
//                                             return SendResponse(res);
//                                         }
//                                     })
//                                 }
//                             })
//                         }
//                     })
//                 }
//             })
//         }
//     })
// }

// /*-----  End of getDashboardData  ------*/