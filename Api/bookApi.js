var Book = require('../model/book');
var genarate = require("../callbackFunction/seq");
module.exports.register = function(req, res) {
    // var data=req.body;
    // var saveData=new Book(data);
    // saveData.save(function(err,success){
    // 	if(err){
    // 		res.send(err);
    // 	}
    // 	else{
    genarate.bookSeqNumber(function(err, count) {
        if (err) {

        } else {
            console.log("count===>", count)
            req.body['seqNo'] = count;
            var data = req.body;
            var saveData = new Book(req.body);
            saveData.save(function(err, success) {
                if (err) {
                    console.log("err")
                    res.send(err);
                } else {
                    console.log("saveData")
                    res.send(success);
                }
            })
        }
    })
}

module.exports.getUserBookList = function(req, res) {

    if (!req.body.hasOwnProperty("userId")) {
        res.send({
            msg: "UserId is required"
        })
    } else if (!req.body.hasOwnProperty("name")) {
        res.send({
            msg: "Name is required"
        })
    } else {
        Book.find({
            "userId": req.body.userId,
            "name": req.body.name
        }, function(err, success) {
            if (err) {
                res.send(err)
            } else if (success) {
                res.send(success)
            } else {
                res.send("userId not found");

            }
        })
    }
}

module.exports.addbook = function(req, res) {
    var bookWithId = req.body;
    var saveData = new Book(bookWithId)
    saveData.save(function(err, success) {
        if (err) {
            res.send(err)
        } else {
            res.send(success)
            console.log("book is added");
        }
    })
}

module.exports.findOneAndUpdate = function(req, res) {
    if (!req.body.hasOwnProperty("name")) {
        res.send("name is required");
    } else if (!req.body.hasOwnProperty("_id")) {
        res.send("_id required");
    } else if (!req.body.hasOwnProperty("userId")) {
        res.send("userId required")
    } else {
        Book.findOneAndUpdate({
            "_id": req.body._id,
            "name": req.body.name
        }, {
            $set: {
                "userId": req.body.userId
            }
        }, function(err, success) {
            if (err) {
                res.send(err)
            } else if (success) {
                res.send(success)
            } else {
                res.send("id not found")
                console.log("msg:id not found");
            }
        })
    }
}


module.exports.booklist = function(req, res) {
    if (!req.body.hasOwnProperty("price")) {
        res.send("price required");
    } else {
        Book.find({
            "price": {
                $gt: req.body.price
            }
        }, function(err, success) {
            if (err) {
                res.send(err)
            } else {
                res.send(success)
            }
        })
    }
}


module.exports.booklist1 = function(req, res) {
    if (!req.body.hasOwnProperty("maxprice") && !req.body.hasOwnProperty("minprice")) {
        res.send("maxprice and minprice are required ");
    } else if (!req.body.hasOwnProperty("minprice")) {
        res.send("minprice required");
    } else if (!req.body.hasOwnProperty("maxprice")) {
        res.send("maxprice required")
    } else {
        Book.find({
            "price": {
                $gt: req.body.minprice,
                $lt: req.body.maxprice
            }
        }, function(err, success) {
            if (err) {
                res.send(err)
            } else {
                res.send(success)
            }
        })
    }
}



module.exports.booklist2 = function(req, res) {
    var price = req.body;
    Book.find({
        "price": {
            $in: [200, 301, 222, 400, 500, 201]
        }
    }, function(err, success) {
        if (err) {
            res.send(err)
        } else {
            res.send(success)
        }
    })
}

module.exports.selectbook = function(req, res) {
    Book.find({}, {
        "author": 1,
        "name": 1
    }, function(err, success) {
        if (err) {
            res.send(err)
        } else {
            res.send(success)
        }
    })
}

module.exports.populate = function(req, res) {
    if (!req.body.hasOwnProperty("_id")) {
        res.send("id is required")
    } else {
        Book.find({
            "_id": req.body._id
        }).populate("userId").exec(function(err, success) {
            if (err) {
                res.send(err)
            } else {
                res.send(success)
            }
        })
    }
}

// db.books.aggregate( [ { $project : { price : 1 , author : 1 } } ] )
//only show _id ,author and price, if _id :0 then id not show in resuls

module.exports.aggregate = function(req, res) {
    Book.aggregate([
        //match price between two value
        // {
        //  $match : { price : { $gt : 200, $lte : 600 } } 
        // },
        // {
        // 	//grouping by author name and sum its price
        //        $group : {
        //        	_id : "$author",
        //           total: { $sum:"$price"} }
        //       },
        {
            $lookup: {
                from: "users", //external collection.
                localField: "userId", //input field., from current collection.  
                foreignField: "_id", //foreign key from external collection,
                as: "usersDetails"
            }
        }

        // sort by total 
        // {
        // 	$sort : {total : 1}
        // }
    ], function(err, success) {
        if (err) {
            res.send(err)
        } else {
            res.send(success)
        }
    })
}