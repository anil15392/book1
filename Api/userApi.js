var User = require('../model/user')
    //for token we require jsonwebtoken //
var jwt = require('jsonwebtoken');
//nexo require for text message
var Nexmo = require('nexmo');
// nodemailer require for text mail
const nodemailer = require('nodemailer');
var generateHash1 = require("../middleware/bcrypt");


var genarate = require("../callbackFunction/seq");

module.exports.signup = function(req, res) {

    //----validation on phoneNumber and password---------//

    if (!req.body.hasOwnProperty("phoneNumber")) {
        res.send("phoneNumber is required")
    }
    // toString return comma-separated list of elements ie('1,2,3')
    else if (req.body.phoneNumber.toString().length != 10) {
        res.send("phoneNumbe must be 10 digit")
    } else if (!req.body.hasOwnProperty("password")) {
        res.send("password is required")
    } else if (req.body.password.length != 8) {
        res.send("password must be 8 charecter")
    }
    // else if(req.body.password==null || req.body.password==" "){
    // 	res.send("password must have some charector")
    // }
    else {
        User.findOne({
            "phoneNumber": req.body.phoneNumber,
            "password": req.body.password
        }, function(err, success) {
            if (err) {
                res.send(err);
            } else if (success) {
                console.log("already registered")
                res.send(success);
            }
            //-----------------------------------------------------------------//
            //---  genarate seqNumber and savedata using callback function ----//
            //-----------------------------------------------------------------//
            else {
                genarate.userSeqNumber(function(err, count) {
                    if (err) {

                    } else {
                        console.log("count===>", count)
                            //if req.body have seqNo key then update data otherwise add key [seqNo] in req.body//
                        req.body['seqNo'] = count;

                        var saveData = new User(req.body);
                        saveData.save(function(err, success) {
                            if (err) {
                                console.log("err")
                                res.send(err);
                            } else {
                                console.log("saveData")
                                res.send(success);
                            }
                        })
                    }
                })
            }
        })
    }
}

// login using validation  ///
module.exports.login = function(req, res) {
    if (!req.body.hasOwnProperty("phoneNumber")) {
        res.send("phoneNumber is required")
    } else if (!req.body.hasOwnProperty("password")) {
        res.send("password is required")
    } else {
        User.findOne({
            "phoneNumber": req.body.phoneNumber,
            "password": req.body.password
        }, function(err, success) {
            if (err) {
                console.log("err")
                res.send(err);
            } else if (success) {
                // console.log("login successfull");
                //genarate token//
                var token = jwt.sign(success.toJSON(), 'anffkk');
                // two step varification 
                var nexmo = new Nexmo({
                    apiKey: "1d609b4f",
                    apiSecret: "D2XBLLYGw0CzPcEh",
                });
                var verifyRequestId = null; // use in the check process
                // generate 4 digit random otp 
                var x = Math.floor((Math.random() * 10000) + 1);
                var msg = "Your otp is :" + x
                nexmo.message.sendSms("NEXMO", "+918572969074", msg, function(err, text) {
                    if (err) {
                        console.log(err)
                        res.send('Oops! There was an error.');
                    } else {
                        console.log(msg);
                        console.log(text);
                        //update request id in user db
                        User.findByIdAndUpdate(success._id, {
                            "$set": {
                                requestId: text.messages[0]['message-id'],
                                otp: x
                            }
                        }, function(err, success) {
                            res.send({
                                "success": success,
                                "token": token
                            });
                        })
                    }
                });
            } else {
                console.log("Register first")
                res.send({
                    "msg": "Register first"
                });
            }
        })
    }
}

// module.exports.verifyOtp=function(req,res){
// 	console.log(req.body);
// 	var nexmo = new Nexmo({
// 				      apiKey: "1d609b4f",
// 				     apiSecret: "D2XBLLYGw0CzPcEh",

// 				  });
// nexmo.verify.check({request_id: req.body.requestId, code:req.body.pin},function(err, result){

//     if(err) {
//     	console.log("err");
//     	res.send(err);
//     } else {
//     	console.log(result);
//       if(result && result.status == '0') { 
//         res.send('Account verified');
//     }
//        else {
//     		res.send(" wrong PIN");
//       }
//     }
//   });
// }

module.exports.verifyOtp = function(req, res) {
    // var nexmo = new Nexmo({
    // 			      apiKey: "1d609b4f",
    // 			     apiSecret: "D2XBLLYGw0CzPcEh",

    // 			  });
    User.findOne({
        requestId: req.body.requestId,
        otp: req.body.otp
    }, function(err, result) {
        if (err) {
            console.log("err");
            res.send(err);
        } else if (result) {
            console.log(result)
            res.send('Account verified');
        } else {
            res.send(" wrong PIN");
        }
    });
}



//verify token--------//
module.exports.verifyToken = function(req, res) {
    if (!req.body.hasOwnProperty("token")) {
        res.send("token required");
    } else {
        jwt.verify(req.body.token, "anffkk", function(err, token) {
            if (err) {
                res.send(err)
            } else {
                res.send(token)

            }
        })
    }
}

//get details by id//

module.exports.getdetails = function(req, res) {
    if (!req.body.hasOwnProperty("_id")) {
        res.send("id is required")
    } else {
        User.findById({
            "_id": req.body._id
        }, function(err, success) {
            if (err) {
                res.send(err)
            } else if (success) {
                res.send(success)
            } else {
                res.send({
                    "msg": "id not found"
                })
            }
        })
    }
}

//------change password------------//

module.exports.changePassword = function(req, res) {
    if (!req.body.hasOwnProperty("phoneNumber")) {
        res.send("phoneNumber is required");
    } else if (!req.body.hasOwnProperty("password")) {
        res.send("password required");
    } else if (!req.body.hasOwnProperty("newpassword")) {
        res.send("newpassword required");
    } else {
        User.findOne({
            "phoneNumber": req.body.phoneNumber
        }, function(err, success) {
            if (err) {
                res.send(err)
            } else if (success) {
                if (success.password == req.body.password) {
                    User.findOneAndUpdate({
                        "phoneNumber": req.body.phoneNumber
                    }, {
                        $set: {
                            "password": req.body.newpassword
                        }
                    }, function(err, success) {
                        if (err) {
                            res.send(err)
                        } else {

                            res.send(success)
                        }
                    })
                } else {
                    res.send("please inter correct password")
                }

            } else {
                res.send("please enter correct userId")
            }
        })
    }
}


// Send a text message using nexmo 
module.exports.textMessage = function(req, res) {
    //Initialize the library
    var nexmo = new Nexmo({
        apiKey: "1d609b4f",
        apiSecret: "D2XBLLYGw0CzPcEh",
    });
    const to = "+918572969074";
    const from = "NEXMO";
    const text = "Hello from Nexmo";
    // nexmo.message.sendSms(sender, recipient, message, options, callback),
    nexmo.message.sendSms(from, to, text, function(err, text) {
        if (err) {
            console.log(err)
            res.send('Oops! There was an error.');

        } else {
            console.log(text);
            res.send(text);
        }
    });
}

// module.exports.sendOtp=function(req,res){
// var nexmo = new Nexmo({
//      apiKey: "1d609b4f",
//      apiSecret: "D2XBLLYGw0CzPcEh",
//     // applicationId: APP_ID,
//     // privateKey: PRIVATE_KEY_PATH,
//   });
// var verifyRequestId = null; // use in the check process
// nexmo.verify.request({number: +918572969074, brand: 'myapp'}, function(err, result) {
//   if(err) { console.error(err); }
//   else {
//     verifyRequestId = result.request_id;
//   }
// });
// }

module.exports.createTestMail = function(req, res) {
    let transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false,
        // service: 'gmail',
        auth: {
            user: 'anil.omnisttechhub@gmail.com', // generated ethereal user
            pass: '123456'
        }
    });
    const mailOptions = {
        from: 'anil.omnisttechhub@gmail.com', // sender address
        to: 'anilyadav01@gmail.com', // list of receivers
        subject: 'test email api', // Subject line
        text: 'Hello world?', // plain text body
        // html: '<p>Your html here</p>'// plain text body
    };
    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {
            res.send(err)
        } else {
            res.send(info);
        }
    });
}