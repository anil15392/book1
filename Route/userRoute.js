var express = require("express");
var router = express.Router();
// require middleware and verify token using next()//
var verify = require("../middleware/verifyToken");
var upload = require('../uploadFile/uploadfile') // uploadFile.file
var userApi = require("../Api/userApi");
router.post('/signup', upload.file, userApi.signup);
router.post('/login', userApi.login);
router.post('/verifyOtp', userApi.verifyOtp);
router.post('/getdetails', verify.Token, userApi.getdetails);
router.post('/changePassword', userApi.changePassword);
router.post('/verifyToken', userApi.verifyToken);
router.get('/textMessage', userApi.textMessage);
router.get('/createTestMail', userApi.createTestMail);


module.exports = router;