const express=require("express");
const router=express.Router();
var verify=require('../middleware/verifyToken');
const bookApi = require("../Api/bookApi");
router.post('/register', bookApi.register);
router.post('/getUserBookList',verify.Token,bookApi.getUserBookList);
router.post('/addbook',bookApi.addbook);
router.post('/findOneAndUpdate',verify.Token,bookApi.findOneAndUpdate);
router.post('/booklist',verify.Token,bookApi.booklist);
router.post('/booklist1',verify.Token,bookApi.booklist1);
router.get('/booklist2',verify.Token,bookApi.booklist2);
router.post('/selectbook',verify.Token,bookApi.selectbook);
router.post('/populate',verify.Token,bookApi.populate);
router.get('/aggregate',bookApi.aggregate);


module.exports=router;