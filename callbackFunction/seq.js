//   callback function use for sequence number//

var User = require('../model/user');
var Book = require('../model/book');
module.exports.userSeqNumber = function(callback) {
    User.count(function(err, count) {
        if (err) callback(err, null);
        else callback(null, count);
    })
}

module.exports.bookSeqNumber = function(callback) {
    Book.count(function(err, count) {
        if (err) callback(err, null);
        else callback(null, count);
    })
}